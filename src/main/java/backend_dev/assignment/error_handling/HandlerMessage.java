package backend_dev.assignment.error_handling;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class HandlerMessage extends Throwable {

    private String statusCode;
    private String message;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp lastUpdate;
    public HandlerMessage(String statusCode, String message, Timestamp lastUpdate){
        super();
        this.statusCode = statusCode;
        this.message = message;
        this.lastUpdate = lastUpdate;

    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = Timestamp.valueOf(lastUpdate);
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }
}

