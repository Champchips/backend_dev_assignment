package backend_dev.assignment.service;


import backend_dev.assignment.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface UserService {
    //    save operation
    User saveUser(User user);
    Optional<User> fetchUserById(Integer userId);
    //   Read operation
    List<User> fetchUserList();

    //    Update operation
    User updateUserById(User user);

    //    Delete operation
    void deleteUserById(Integer userId);

    // Thread Create Multiple User operation
    CompletableFuture<List<User>> saveMultipleUser(MultipartFile file);
    // Thread Find Multiple User operation
    CompletableFuture<List<User>> findAllUsers();

}


