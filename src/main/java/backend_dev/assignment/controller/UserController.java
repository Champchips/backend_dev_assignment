package backend_dev.assignment.controller;

import backend_dev.assignment.error_handling.NotFoundUserException;
import backend_dev.assignment.error_handling.RequiredDataException;
import backend_dev.assignment.model.User;
import backend_dev.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/create")
    public ResponseEntity<User> saveUser(@RequestBody User user) throws RequiredDataException {
        String errorMessage = "Required: ";
        if (user.getUsername() == null || user.getEmail() == null) {
            if (user.getUsername() == null) {
                errorMessage += "Username ";
            }
            if (user.getEmail() == null) {
                errorMessage += "Email ";
            }
            throw new RequiredDataException(errorMessage);
        }
        userService.saveUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping(value = "/find/{id}")
    public User fetchUserById(@PathVariable("id") Integer userId) throws NotFoundUserException {
        Optional<User> user = userService.fetchUserById(userId);
        try {
            return user.get();
        } catch (RuntimeException e) {
            throw new NotFoundUserException("User id " + userId + " not found");
        }
    }

    @GetMapping(value = "/find")
    public List<User> fetchUserList() {
        return userService.fetchUserList();
    }

    @PutMapping(value = "/update/{id}")
    public User updateUserById(@RequestBody User user, @PathVariable("id") Integer userId) {
        user.setId(userId);
        return userService.updateUserById(user);
    }

    @PatchMapping(value = "/update/{id}")
    public User updatePartialUserById(@RequestBody User partialUser, @PathVariable("id") Integer userId) {
        Optional<User> user = userService.fetchUserById(userId);
        User edited_user = user.get();
        if (partialUser.getUsername() != null) {
            edited_user.setUsername(partialUser.getUsername());
        }
        if (partialUser.getEmail() != null) {
            edited_user.setEmail(partialUser.getEmail());
        }
        if (partialUser.getPhoto_url() != null) {
            edited_user.setPhoto_url(partialUser.getPhoto_url());
        }
        return userService.updateUserById(edited_user);
    }

    @DeleteMapping(value = "/delete/{id}")
    public String deleteUserById(@PathVariable("id") Integer userId) {
        userService.deleteUserById(userId);

        return "Deleted Successfully";
    }

    @PostMapping(value = "/createMultiple/", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = "application/json")
    public ResponseEntity saveUsers(@RequestParam(value = "files") MultipartFile[] files) {
        for (MultipartFile file : files) {
            userService.saveMultipleUser(file);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/findAll", produces = "application/json")
    public CompletableFuture<ResponseEntity> findAllUsers() {
        return userService.findAllUsers().thenApply(ResponseEntity::ok);

    }
    @GetMapping(value = "/findAllByThread", produces = "application/json")
    public ResponseEntity getUsers(){
        CompletableFuture<List<User>> users_1 = userService.findAllUsers();
        CompletableFuture<List<User>> users_2 = userService.findAllUsers();
        CompletableFuture<List<User>> users_3 = userService.findAllUsers();
        CompletableFuture.allOf(users_1,users_2,users_3).join();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}


