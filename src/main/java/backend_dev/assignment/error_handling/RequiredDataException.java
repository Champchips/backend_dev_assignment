package backend_dev.assignment.error_handling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class RequiredDataException extends RuntimeException {
    public RequiredDataException(String message) {
        super(message);
    }

}
