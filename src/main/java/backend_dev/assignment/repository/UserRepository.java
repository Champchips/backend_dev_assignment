package backend_dev.assignment.repository;

import backend_dev.assignment.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query(value = "SELECT * from svc_backend_team.userdata WHERE username = ?1", nativeQuery = true)
    Optional<User> findByUser (String username);

}
