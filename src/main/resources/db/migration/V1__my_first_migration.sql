CREATE TABLE userdata
(
    id        INTEGER NOT NULL,
    username  VARCHAR(255),
    email     VARCHAR(255),
    photo_url VARCHAR(255),
    CONSTRAINT pk_userdata PRIMARY KEY (id)
);