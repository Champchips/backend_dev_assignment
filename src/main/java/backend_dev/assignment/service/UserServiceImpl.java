package backend_dev.assignment.service;

import backend_dev.assignment.error_handling.DatabaseConnectionException;
import backend_dev.assignment.model.User;
import backend_dev.assignment.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User saveUser(User user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new DatabaseConnectionException("Can not connect Database");
        }

    }

    @Override
    public Optional<User> fetchUserById(Integer userId) {
        try {
            return userRepository.findById(userId);
        } catch (Exception e) {
            throw new DatabaseConnectionException("Can not connect Database");
        }

    }

    @Override
    public List<User> fetchUserList() {
        try {
            return userRepository.findAll();
        } catch (Exception e) {
            throw new DatabaseConnectionException("Can not connect Database");
        }

    }

    @Override
    public User updateUserById(User user) {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new DatabaseConnectionException(e.toString());
        }

    }

    @Override
    public void deleteUserById(Integer userId) {
        try {
            userRepository.deleteById(userId);
        } catch (Exception e) {
            throw new DatabaseConnectionException("Can not connect Database");
        }

    }

    Object target;
    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Async
    @Override
    public CompletableFuture<List<User>> saveMultipleUser(MultipartFile file) {
        long start = System.currentTimeMillis();
        try {
            List<User> users = parseCSVFile(file);
            logger.info("saving List of Users of size {}", users.size(), "", Thread.currentThread().getName());
            users = userRepository.saveAll(users);
            long end = System.currentTimeMillis();
            logger.info("Total time {}", (end - start));
            return CompletableFuture.completedFuture(users);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Async
    @Override
    public CompletableFuture<List<User>> findAllUsers() {
        logger.info("Get List of Users by " + Thread.currentThread().getName());
        List<User> users = userRepository.findAll();
        return CompletableFuture.completedFuture(users);
    }

    private List<User> parseCSVFile(final MultipartFile file) throws Exception {
        final List<User> users = new ArrayList<>();
        try {
            try (final BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
                String line;
                while ((line = br.readLine()) != null) {
                    final String[] data = line.split(",");
                    final User user = new User();
                    user.setUsername(data[0]);
                    user.setEmail(data[1]);
                    user.setPhoto_url(data[2]);
                    users.add(user);
                }
                return users;
            }
        } catch (final IOException e) {
            logger.error("Failed to parse CSV file {}", e);
            throw new Exception("Failed to parse CSV file {}", e);
        }
    }
}

