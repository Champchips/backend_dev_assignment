package backend_dev.assignment.service;

import backend_dev.assignment.model.User;
import backend_dev.assignment.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;
    private User user1, user2;

    @BeforeEach
    public void setup() {
        user1 = new User(1, "champchips", "champchips@email.com", "champchips.jpg");
        user2 = new User(2, "chicken", "chicken@email.com", "chicken.jpg");
    }

    @DisplayName("Unit Test for saveUser Method")
    @Test
    public void saveUserTest() {
        // Precondition
        given(userRepository.save(user1)).willReturn(user1);


        // We're going to Test
        User createUser = userService.saveUser(user1);
        System.out.println(createUser);

        // Verify
        assertThat(createUser).isNotNull();

    }

    @DisplayName("Unit Test for fetchUserList Method")
    @Test
    public void fetchUserListTest() {
        //Precondition
        given(userRepository.findAll()).willReturn(List.of(user1, user2));

        // We're going to Test
        List<User> userList = userService.fetchUserList();
        System.out.println(userList);

        // Verify
        assertThat(userList).isNotNull();
        assertThat(userList.size()).isEqualTo(2);

    }

    @DisplayName("Unit Test for fetchUserById Method")
    @Test
    public void fetchUserByIdTest() {
        //Precondition
        int id = 1;
        given(userRepository.findById(id)).willReturn(Optional.of(user1));

        // We're going to Test
        User user = userService.fetchUserById(user1.getId()).get();
        System.out.println(user);

        // Verify
        assertThat(user).isNotNull();

    }

    @DisplayName("Unit Test for updateUserById Method")
    @Test
    public void updateUserByIdTest() {
        //Precondition
        given(userRepository.save(user1)).willReturn(user1);
        user1.setUsername("updateUsername");
        user1.setEmail("update@email.com");

        // We're going to Test
        User updateUser = userService.updateUserById(user1);
        System.out.println(updateUser);
        // Verify
        assertThat(updateUser.getUsername()).isEqualTo("updateUsername");
        assertThat(updateUser.getEmail()).isEqualTo("update@email.com");


    }

    @DisplayName("Unit Test for deleteById Method")
    @Test
    public void deleteUserByIdTest() {
        // given - precondition or setup
        int id = 1;

        willDoNothing().given(userRepository).deleteById(id);

        // when -  action or the behaviour that we are going test
        userService.deleteUserById(id);

        // then - verify the output
        verify(userRepository, times(1)).deleteById(id);
    }

}
