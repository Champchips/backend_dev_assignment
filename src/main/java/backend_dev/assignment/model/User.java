package backend_dev.assignment.model;

// Java Program to Demonstrate Department File

// Importing package module to this code fragment

import lombok.*;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@Table(name = "userdata")
@AllArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue
    private int id;
    @Column(name = "username")
    @NotBlank
    private String username;
    @Column(name = "email")
    @NotBlank
    private String email;
    @Column(name = "photo_url")
    private String photo_url;

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }


//    @Override
//    public String toString() {
//        return "User [id=" + this.getId() + ", username=" + this.getUsername() + ", email=" + this.getEmail() + "]";
//    }
}

