package backend_dev.assignment.controller;

import backend_dev.assignment.model.User;
import backend_dev.assignment.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    UserService userService;


    @DisplayName("Unit test for saveUserAPI")
    @Test
    public void saveTest() throws Exception {
        //Setup Data for Test and Precondition
        User user_test = User.builder().id(50).username("champchips_test").email("champchips_test@email.com").photo_url("test_profile.jpg").build();
        given(userService.saveUser(any(User.class))).willAnswer((invocation) -> invocation.getArgument(0));

        //Action we're going to Test
        ResultActions response = mockMvc.perform(post("/api/v1/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user_test)));

        //Verify the result
        response.andDo(print()).andExpect(status().isCreated())
                .andExpect(jsonPath("$.username",
                        is(user_test.getUsername())))
                .andExpect(jsonPath("$.email",
                        is(user_test.getEmail())))
                .andExpect(jsonPath("$.photo_url",
                        is(user_test.getPhoto_url())));
    }

    @DisplayName("Unit Test for fetchUserListAPI")
    @Test
    public void fetchUserListTest() throws Exception {
        //Setup Data for Test and Precondition
        List<User> userList = new ArrayList<>();
        userList.add(User.builder().id(1).username("chipper1").email("chipper1@email.com").photo_url("chipper1_image.jpg").build());
        userList.add(User.builder().id(2).username("chipper2").email("chipper2@email.com").photo_url("chipper2_image.jpg").build());
        given(userService.fetchUserList()).willReturn(userList);

        //Action we're going to Test
        ResultActions response = mockMvc.perform(get("/api/v1/user/find"));

        //Verify the result
        response.andExpect(status().isOk()).andDo(print()).andExpect(jsonPath("$.size()", is(userList.size())));
    }

    @DisplayName("Unit Test for fetchUserById Not found Case")
    @Test
    public void notfound_fetchUserByIdTest() throws Exception {
        //Setup Data for Test and Precondition
        int id = 0;
        given(userService.fetchUserById(id)).willReturn(Optional.empty());

        //Action we're going to Test
        ResultActions resultActions = mockMvc.perform(get("/api/v1/user/find/{id}", id));


        //Verify the result
        resultActions.andExpect(status().isNotFound()).andDo(print());

    }

    @DisplayName("Unit Test for fetchUserById Valid Case")
    @Test
    public void valid_fetchUserByIdTest() throws Exception {
        //Setup Data for Test and Precondition
        User user_test = User.builder().id(0).username("chipper_test_by_id").email("chipper_test_by_id@email.com").photo_url("chipper_test_by_id_image.jpg").build();
        int id = 0;
        given(userService.fetchUserById(id)).willReturn(Optional.of(user_test));

        //Action we're going to Test
        ResultActions resultActions = mockMvc.perform(get("/api/v1/user/find/{id}", id));


        //Verify the result
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username",
                        is(user_test.getUsername())))
                .andExpect(jsonPath("$.email",
                        is(user_test.getEmail())))
                .andExpect(jsonPath("$.photo_url",
                        is(user_test.getPhoto_url())));

    }

    @DisplayName("Unit Test for updateAllUserData")
    @Test
    public void updateUserByIdTest() throws Exception {
        //Setup Data for Test and Precondition

        int id = 0;
        User createdUser = User.builder().username("chipper_test").email("chipper_test@email.com").photo_url("chipper_test_image.jpg").build();

        User updatedUser = User.builder().username("chipper_test_update").email("chipper_test_update@email.com").photo_url("chipper_test_update_image.jpg").build();

        given(userService.fetchUserById(id)).willReturn(Optional.of(createdUser));
        given(userService.updateUserById(any(User.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));

        //Action we're going to Test
        ResultActions response = mockMvc.perform(put("/api/v1/user/update/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedUser)));


        //Verify the result
        response.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username",
                        is(updatedUser.getUsername())))
                .andExpect(jsonPath("$.email",
                        is(updatedUser.getEmail())))
                .andExpect(jsonPath("$.photo_url",
                        is(updatedUser.getPhoto_url())));

    }

    @DisplayName("Unit Test for deleteUserById")
    @Test
    public void deleteUserByIdTest() throws Exception {
        //Setup Data for Test and Precondition
        int id = 1;
        willDoNothing().given(userService).deleteUserById(id);

        //Action we're going to Test
        ResultActions response = mockMvc.perform(delete("/api/v1/user/delete/{id}", id));

        //Verify the result
        response.andExpect(status().isOk()).andDo(print());
    }


}
