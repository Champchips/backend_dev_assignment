package backend_dev.assignment.repository;


import backend_dev.assignment.model.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Test
    @Order(1)
    @Rollback(value = false)
    public void saveUserTest(){

        User user = User.builder()
                .username("Champchips")
                .email("champchips@email.com")
                .photo_url("champchips.jpg")
                .build();

        userRepository.save(user);

        Assertions.assertThat(user.getId()).isGreaterThan(0);

    }
    @Test
    @Order(2)
    public void findByIdTest(){

        User user = userRepository.findById(1).get();
        System.out.println(user);

        Assertions.assertThat(user.getId()).isEqualTo(1);

    }

    @Test
    @Order(3)
    public void findAllTest(){

        List<User> users = userRepository.findAll();

        Assertions.assertThat(users.size()).isGreaterThan(0);
    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updateTest(){

        User user = userRepository.findById(1).get();

        user.setUsername("TestUpdate");

        User userUpdated =  userRepository.save(user);

        Assertions.assertThat(userUpdated.getUsername()).isEqualTo("TestUpdate");

    }

    @Test
    @Order(5)
    @Rollback(value = false)
    public void deleteTest(){

        User user= userRepository.findById(1).get();

        userRepository.delete(user);

        User user1 = null;

        Optional<User> optionalUser= userRepository.findByUser("TestUpdate");

        if(optionalUser.isPresent()){
            user1 = optionalUser.get();
        }

        Assertions.assertThat(user1).isNull();
    }

}
